# Generic Matrix Library (GML) #

The intent of this library is to provide a generic template library to handle matrix manipulations.

This library needs some C++11 features and has been tested on MSVC 2013 and GCC 4.8 (minimal requirement). This library has no support for non C++11 enabled compilers.

A documentation of the library can by found here: [http://www.polytech.univ-nantes.fr/wikinfo/gmt/](http://www.polytech.univ-nantes.fr/wikinfo/gmt/).
