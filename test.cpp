#include <iostream>
#include "Matrix/CMat_Swizzle.hpp"
#include "Matrix/CMat3D.hpp"

using namespace std;
using namespace OpenUtility;

#define PrintTab(arg) _PrintTab(#arg,arg)

template<class T,int R,int C>
void _PrintTab(const char *name,T(&tab)[R][C])
{
	cout << "Tab (" << name << "):" << endl;
	for (unsigned int i=0;i<R;i++)
		for (unsigned int j=0;j<C;j++)
			cout << tab[i][j] << ' ';
	cout << endl << endl;
}

template<class T,int R>
void _PrintTab(const char *name,T(&tab)[R])
{
	cout << "Tab (" << name << "):" << endl;
	for (unsigned int i=0;i<R;i++)
		cout << tab[i] << ' ';
	cout << endl << endl;
}

#define PrintStorage(arg) _PrintStorage(#arg,arg)

template<class T,int R,int C,typename Derived>
void _PrintStorage(const char *name,SStorage<T,R,C,Derived> &obj)
{
	cout << "Storage (" << name << "):" << endl;
	for (unsigned int i=0;i<R;i++)
	{
		for (unsigned int j=0;j<C;j++)
			cout << obj(i,j) << ' ';
		cout << endl;
	}
	cout << endl << endl;
}

int main()
{
	const unsigned int R=3,C=4;

	CMat<double,R,C> mat1(MatrixInit::No);
	cout << "mat1\n" << mat1 << endl << endl;
	cout << "sizeof(mat1): " << sizeof(mat1) << endl << endl;

	CMat<int,R,C> mat2(1,2,3,4,5,6,7,8,9,10,11,12);
	cout << "mat2\n" << mat2 << endl << endl;
	cout << "sizeof(mat2): " << sizeof(mat2) << endl;
	cout << "mat2(1)= " << mat2(1) << endl << endl;
	cout << "mat2(1,1)= " << mat2(1,1) << endl << endl;
	cout << "mat2.GetVal<1>()= " << mat2.GetVal<1>() << endl << endl;
	cout << "mat2.GetVal<1,1>()= " << mat2.GetVal<1,1>() << endl << endl;

	mat2+=3;
	cout << "mat2\n" << mat2 << endl << endl;

	double tab[R][C];
	PrintTab(tab);

	SProxyArrayStorage<double,R,C> proxy=tab;
	cout << "sizeof(proxy): " << sizeof(proxy) << endl << endl;
	CMat<double,R,C,SProxyArrayStorage<double,R,C>> matProxy1(proxy);
	cout << "matProxy1\n" << matProxy1 << endl << endl;
	PrintTab(tab);
	cout << "sizeof(matProxy1): " << sizeof(matProxy1) << endl << endl;

	CMat<double,R,C,SProxyArrayStorage<double,R,C>> matProxy2(tab,1,2,3,4,5,6,7,8,9,10,11,12);
	cout << "matProxy2\n" << matProxy2 << endl << endl;
	PrintTab(tab);

	CMat<double,R,C,SProxyArrayStorage<double,R,C>> matProxy3(tab,MatrixInit::No);
	cout << "matProxy3\n" << matProxy3 << endl << endl;
	PrintTab(tab);

	CMat<double,R,C,SProxyArrayStorage<double,R,C>> matProxy4(tab);
	cout << "matProxy4\n" << matProxy4 << endl << endl;
	PrintTab(tab);
	
	tab[1][2]=5;
	PrintTab(tab);
	auto matProxy5=make_matrix(tab);
	cout << "matProxy5\n" << matProxy5 << endl << endl;

	matProxy1.Set(2,3,4,0,1,8,9,4,10,2,1,5);
	PrintTab(tab);

	cout << "matProxy1T\n" << Transpose(matProxy1) << endl << endl << endl;

	cout << "matProxy1+mat2\n" << matProxy1+mat2 << endl << endl;
	cout << "matProxy1-mat2\n" << matProxy1-mat2 << endl << endl;
	cout << "-mat2\n" << -mat2 << endl << endl;

	auto mat2T=MView::Transpose(mat2);
	cout << "mat2\n" << mat2 << endl;
	cout << "mat2T\n" << mat2T << endl << endl;
	cout << "mat2 (0 ... " << R*C-1 << ")" << endl;
	for (unsigned int i=0;i<R*C;i++) cout << ' ' << mat2(i);
	cout << endl;
	cout << "mat2T (0 ... " << R*C-1 << ")" << endl;
	for (unsigned int i=0;i<R*C;i++) cout << ' ' << mat2T(i);
	cout << endl << endl;

	CMat<float,2,3,SDenseStorage<double,2,3,SRowMajor>> mat_mul1(MatrixInit::No);
	CMat<double,3,2> mat_mul2(MatrixInit::No);
	mat_mul1.Set(1,4,2,3,0,-1);
	mat_mul2.Set(5,2,3,1,3,4);
	cout << "mat_mul1\n" << mat_mul1 << endl << endl;
	cout << "mat_mul2\n" << mat_mul2 << endl << endl;
	auto mat_mul_res=mat_mul1*mat_mul2;
	cout << "mat_mul1*mat_mul2\n" << mat_mul_res << endl << endl;
	cout << "mat_mul2*mat_mul1\n" << mat_mul2*mat_mul1 << endl << endl;

	CMat<double,2,3> mat_copy1(MatrixInit::No);
	CMat<double,2,3,SDenseStorage<double,2,3,SRowMajor>> mat_copy2(1,2,3,4,5,6);
	cout << "mat_copy1\n" << mat_copy1 << endl << endl;
	cout << "mat_copy2\n" << mat_copy2 << endl << endl;
	mat_copy1=mat_copy2;
	cout << "mat_copy1\n" << mat_copy1 << endl << endl;
	cout << "mat_copy2\n" << mat_copy2 << endl << endl;

	CVec<int,3> vec_1(3,2,1);
	cout << "vec_1\n" << vec_1 << endl << endl;
	cout << "vec_1*vec_1T\n" << vec_1*Transpose(vec_1) << endl << endl;
	cout << "vec_1T*vec_1\n" << Transpose(vec_1)*vec_1 << endl << endl;

	CVec<double,3> vec_2(3.3,2.2,1.1);
	cout << "vec_2\n" << vec_2 << endl << endl;
	cout << "vec_2*vec_1T\n" << vec_2*Transpose(vec_1) << endl << endl;
	cout << "vec_2*vec_1\n" << Transpose(vec_2)*vec_1 << endl << endl;

	CMat<double,3,4> mat_equal1(1,2,3,4,5,6,7,8,9,10,11,12);
	CMat<double,4,3> mat_equal2(1,2,3,4,5,6,7,8,9,10,11,12);
	int tab2[3][4];
	CMat<int,3,4,SProxyArrayStorage<int,3,4>> mat_equal3(tab2,1,2,3,4,5,6,7,8,9,10,11,12);
	cout << "mat_equal1\n" << mat_equal1 << endl << endl;
	cout << "mat_equal2\n" << mat_equal2 << endl << endl;
	cout << "mat_equal3\n" << mat_equal3 << endl << endl;

	cout << "mat_equal1==mat_equal2 : " << (mat_equal1==mat_equal2 ? "true" : "false") << endl;
	cout << "mat_equal1!=mat_equal2 : " << (mat_equal1!=mat_equal2 ? "true" : "false") << endl;
	cout << "mat_equal1==mat_equal3 : " << (mat_equal1==mat_equal3 ? "true" : "false") << endl;
	cout << "mat_equal1!=mat_equal3 : " << (mat_equal1!=mat_equal3 ? "true" : "false") << endl;
	cout << "mat_equal3==mat_equal1 : " << (mat_equal3==mat_equal1 ? "true" : "false") << endl << endl;

	CVec<double,3> vec_3(1,3,-5);
	CVec<double,3> vec_4(4,-2,-1);
	cout << "vec_3\n" << vec_3 << endl << endl;
	cout << "vec_4\n" << vec_4 << endl << endl;
	cout << "vec_3 . vec_4 = " << Dot(vec_3,vec_4) << endl << endl;

	CMat<double,5,5> mat3(MatrixInit::Null);
	cout << "mat3\n" << mat3 << endl << endl;
	mat3.SetIdentity();
	cout << "mat3\n" << mat3 << endl << endl;

	CMat<int,5,5,SDenseStorage<int,5,5,SRowMajor>> mat4(MatrixSquareInit::Identity);
	cout << "mat4\n" << mat4 << endl << endl;
	mat4.Set(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25);
	cout << "mat4\n" << mat4 << endl << endl;

	mat3*=mat4;
	cout << "mat3 (after mat3*=mat4)\n" << mat3 << endl << endl;

	mat3.Transpose();
	cout << "mat3 (after mat3.Transpose())\n" << mat3 << endl << endl;

	double tab3[5][5];
	CMat<int,5,5,SProxyArrayStorage<double,5,5>> mat5(tab3,MatrixSquareInit::Identity);
	cout << "mat5\n" << mat5 << endl << endl;
	PrintTab(tab3);

	mat5*=mat3;
	cout << "mat5 (after mat5*=mat3)\n" << mat5 << endl << endl;
	PrintTab(tab3);

	CMat4x4<double> mat6(MatrixInit::No);
	cout << "mat6\n" << mat6 << endl << endl;
	mat6.SetTranslate(1,2,3);
	cout << "mat6\n" << mat6 << endl << endl;

	cout << "mat6.GetMatrix()" << endl;
	for (unsigned int i=0;i<4*4;i++)
		cout << " " << mat6.GetMatrix()[i];
	cout << endl << endl;

	double tab4[4][4];
	CMat4x4<double,SProxyArrayStorage<double,4,4>> mat7(tab4,MatrixInit::No);
	mat7.SetRotate(60,0,1,0);
	cout << "mat7\n" << mat7 << endl << endl;
	PrintTab(tab4);

	mat7.SetScale(2,3,4);
	cout << "mat7\n" << mat7 << endl << endl;
	PrintTab(tab4);

	CMat4x4<double> mat4x4_t(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
	cout << "mat4x4_t\n" << mat4x4_t << endl << endl;
	mat4x4_t.Transpose();
	cout << "mat4x4_t T\n" << mat4x4_t << endl << endl;

	CMat4x4<double,SDenseStorage<double,4,4,SRowMajor>> mat_inv(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
	cout << "mat_inv\n" << mat_inv << endl << endl;
	cout << "(mat_inv)^(-1)\n" << mat_inv.Invert() << endl << endl;

	mat_inv.Set(5,3,1,9,8,10,14,3,5,-7,25,1,3,6,18,3);
	cout << "mat_inv\n" << mat_inv << endl << endl;
	cout << "(mat_inv)^(-1)\n" << mat_inv.Invert() << endl << endl;
	cout << "(mat_inv)^(-1)^(-1)\n" << mat_inv.Invert() << endl << endl;

	CMat4x4<double,SDenseStorage<double,4,4,SRowMajor>> mat_transR(MatrixInit::No);
	CMat4x4<double,SDenseStorage<double,4,4,SColumnMajor>> mat_transC(MatrixInit::No);

	mat_transR.SetTranslate(1,2,3);
	cout << "mat_transR\n" << mat_transR << endl << endl;
	cout << "(mat_transR)^(-1)\n" << mat_transR.Invert() << endl << endl;
	cout << "(mat_transR)^(-1)^(-1)\n" << mat_transR.Invert() << endl << endl;

	mat_transC.SetTranslate(1,2,3);
	cout << "mat_transC\n" << mat_transC << endl << endl;
	cout << "(mat_transC)^(-1)\n" << mat_transC.Invert() << endl << endl;
	cout << "(mat_transC)^(-1)^(-1)\n" << mat_transC.Invert() << endl << endl;

	mat_transR.SetRotate(90,0,1,0);
	cout << "mat_transR\n" << mat_transR << endl << endl;
	cout << "(mat_transR)^(-1)\n" << mat_transR.Invert() << endl << endl;
	cout << "(mat_transR)^(-1)^(-1)\n" << mat_transR.Invert() << endl << endl;

	mat_transC.SetRotate(90,0,1,0);
	cout << "mat_transC\n" << mat_transC << endl << endl;
	cout << "(mat_transC)^(-1)\n" << mat_transC.Invert() << endl << endl;
	cout << "(mat_transC)^(-1)^(-1)\n" << mat_transC.Invert() << endl << endl;

	CVec4<double> vec4(4,3,2,1);
	cout << "vec4\n" << vec4 << endl << endl;
	cout << "sizeof(vec4): " << sizeof(vec4) << endl << endl;
	vec4.Set(1,2,3,4);
	cout << "vec4\n" << vec4 << endl << endl;
	cout << "vec4[0]: " << vec4[0] << "\tvec4[1]: " << vec4[1] << "\tvec4[2]: " << vec4[2] << "\tvec4[3]: " << vec4[3] << endl;
	cout << "vec4.x(): " << vec4.x() << "\tvec4.y(): " << vec4.y() << "\tvec4.z(): " << vec4.z() << "\tvec4.w(): " << vec4.w() << endl;
	cout << "vec4.r(): " << vec4.r() << "\tvec4.g(): " << vec4.g() << "\tvec4.b(): " << vec4.b() << "\tvec4.a(): " << vec4.a() << endl << endl;

	auto vec4T=Transpose(vec4);
	cout << "vec4T\n" << vec4T << endl << endl;
	cout << "vec4T*vec4\n" << vec4T*vec4 << "\n" << endl << endl;
	cout << "vec4*vec4T\n" << vec4*vec4T << endl << endl;

	double tab5[1][4];
	CVecT<double,4,SProxyArrayStorage<double,1,4>> vec4_tab(tab5);
	cout << "vec4_tab\n" << vec4_tab << endl << endl;

	cout << "vec4_tab.GetMatrix()" << endl;
	for (unsigned int i=0;i<4;i++)
		cout << " " << vec4_tab.GetMatrix()[i];
	cout << endl << endl;

	PrintTab(tab5);
	cout << "sizeof(tab5): " << sizeof(tab5) << endl << endl;
	cout << "sizeof(vec4_tab): " << sizeof(vec4_tab) << endl << endl;

	double tab6[6];
	auto vec4_tab2=make_matrix(tab6);
	vec4_tab2.Set(1,2,3,4,5,6);
	cout << "vec4_tab2\n" << vec4_tab2 << endl << endl;
	PrintTab(tab6);

	auto vec4_tab3=make_matrix<2,3>(tab6);
	cout << "vec4_tab3\n" << vec4_tab3 << endl << endl;
	PrintTab(tab6);

	auto vec4_tab4=make_matrix<1,4>((double*)tab6);
	vec4_tab4.g()=1;
	cout << "vec4_tab4\n" << vec4_tab4 << endl << endl;
	PrintTab(tab6);

	SDenseStorage<double,2,3,SRowMajor> store_d1;
	PrintStorage(store_d1);

	for (unsigned int i=0;i<2*3;i++) store_d1(i)=i;
	PrintStorage(store_d1);

	SViewMatrixStorage<double,1,1,1,2,2,3,SDenseStorage<double,2,3,SRowMajor>> store_p1(store_d1);
	cout << "sizeof(store_p1)=" << sizeof(store_p1) << endl;
	PrintStorage(store_p1);
	store_p1(0,0)=10;
	PrintStorage(store_p1);
	store_p1(0,1)=11;
	PrintStorage(store_p1);
	PrintStorage(store_d1);

	SDenseStorage<double,1,2,SRowMajor> store_d2;
	store_d2(0)=100;store_d2(1)=200;
	PrintStorage(store_d2);

	store_p1=store_d2;
	PrintStorage(store_d2);
	PrintStorage(store_p1);
	PrintStorage(store_d1);

	CMat<double,5,6> mat8(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30);
	cout << "mat8\n" << mat8 << endl << endl;
	auto mat9=MView::Matrix<1,1,4,4>(mat8);
	cout << "mat9\n" << mat9 << endl << endl;
	cout << "sizeof(mat8)=" << sizeof(mat8) << endl;
	cout << "sizeof(mat9)=" << sizeof(mat9) << endl << endl;

	mat9(0,1)=-5;
	cout << "mat8\n" << mat8 << endl << endl;
	cout << "mat9\n" << mat9 << endl << endl;

	mat9.SetRotate(50,1,2,3);
	cout << "mat8\n" << mat8 << endl << endl;
	cout << "mat9\n" << mat9 << endl << endl;

	SDenseStorage<double,4,1> store_d3;
	store_d3(0)=1;
	store_d3(1)=2;
	store_d3(2)=3;
	store_d3(3)=4;
	PrintStorage(store_d3);

	auto vec_5=MView::Col<2>(mat8);
	cout << "mat8\n" << mat8 << endl << endl;
	cout << "vec_5\n" << vec_5 << endl << endl;

	auto vec_6=MView::Col<2,1,2>(mat8);
	cout << "mat8\n" << mat8 << endl << endl;
	cout << "vec_6\n" << vec_6 << endl << endl;

	auto vec_7=MView::Row<2>(mat8);
	cout << "mat8\n" << mat8 << endl << endl;
	cout << "vec_7\n" << vec_7 << endl << endl;

	auto vec_8=MView::Row<2,1,2>(mat8);
	cout << "mat8\n" << mat8 << endl << endl;
	cout << "vec_8\n" << vec_8 << endl << endl;

	auto vec_9=MView::Diag(mat4);
	cout << "mat4\n" << mat4 << endl << endl;
	cout << "vec_9\n" << vec_9 << endl << endl;

	vec_9[2]=100;
	cout << "mat4\n" << mat4 << endl << endl;
	cout << "vec_9\n" << vec_9 << endl << endl;

	(vec_9%SW<0,3>()).SetNull();
	cout << "mat4\n" << mat4 << endl << endl;
	cout << "vec_9\n" << vec_9 << endl << endl;

	auto sw1=MView::SwizzleL<MView::GenSwizzleTypeList<_Y_,_X_,_0_,_1_>>(vec4);
	cout << "vec4\n" << vec4 << endl;
	cout << "sw1\n" << sw1 << endl << endl;

	sw1.x()=10;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw1\n" << sw1 << endl << endl;

	sw1.y()=20;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw1\n" << sw1 << endl << endl;

	auto sw2=MView::Swizzle<_X_,_0_>(vec4);
	cout << "vec4\n" << vec4 << endl;
	cout << "sw2\n" << sw2 << endl << endl;

	auto sw2_2=MView::Swizzle<1,0>(vec4);
	cout << "vec4\n" << vec4 << endl;
	cout << "sw2_2\n" << sw2_2 << endl << endl;


#ifdef GEN_SWIZZLE_FUNCTION
	auto sw3=vec4%XXY;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw3\n" << sw3 << endl << endl;

	auto sw4=vec4%RR;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw4\n" << sw4 << endl << endl;

	auto sw5=vec4%ZYX1;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw5\n" << sw5 << endl << endl;

	auto sw6=vec4%WZYX;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw6\n" << sw6 << endl << endl;
#endif

	auto sw7=vec4%SWT<_X_,_0_>();
	cout << "vec4\n" << vec4 << endl;
	cout << "sw7\n" << sw7 << endl << endl;

#ifndef _MSC_VER
	auto sw8=vec4%SW<_X_,_X_>;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw8\n" << sw8 << endl << endl;

	auto sw9=vec4%SW<1,0,2>;
	cout << "vec4\n" << vec4 << endl;
	cout << "sw9\n" << sw9 << endl << endl;
#endif

	auto sw10=vec4%SWI<1,0,3>();
	cout << "vec4\n" << vec4 << endl;
	cout << "sw10\n" << sw10 << endl << endl;

	auto sw11=vec4%SWI<1>();
	cout << "vec4\n" << vec4 << endl;
	cout << "sw11\n" << sw11 << endl;
	cout << "sw11.x()=" << sw11.x() << endl << endl;
	sw11.SetIdentity();
	cout << "vec4\n" << vec4 << endl;
	cout << "sw11\n" << sw11 << endl;

	return(0);
}
