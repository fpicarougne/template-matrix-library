//  OpenUtility CMat3D_Impl.hpp header file

//  (C) Copyright Fabien Picarougne 2014.
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

#ifdef _OU_CMat3D_h
	#ifndef _OU_CMat3D_Impl_h
		#define _OU_CMat3D_Impl_h

#include <cmath>
#include <limits>

static const double pi=3.14159265358979323846264338328;

//--------------------------------------------------------------------------------------------------------
// General operations

template<class T1,class F1,class T2,class F2,class T3,class F3>
OpenUtility::CMat<T3,3,1,F3>& OpenUtility::Vec3CrossProd(const CMat<T1,3,1,F1> &v,const CMat<T2,3,1,F2> &w,CMat<T3,3,1,F3> &r)
{
	r.x()=v.y()*w.z()-v.z()*w.y();
	r.y()=v.z()*w.x()-v.x()*w.z();
	r.z()=v.x()*w.y()-v.y()*w.x();
	return(r);
}

template<class T1,class F1,class T2,class F2>
OpenUtility::CMat<T1,3,1,F1>& OpenUtility::Vec3Rotate(CMat<T1,3,1,F1> &v,const CMat<T2,3,1,F2> &r,T1 angle)
{
	T1 theta=static_cast<T1>(angle/(180*pi));
	T1 c=cos(theta);
	T1 s=sin(theta);
	T1 t=1-c;
	CMat<T1,3,1,F1> tmp=v;

	v.x()=(t*r.x()*r.x()+c)*tmp.x() + (t*r.x()*r.y()+s*r.z())*tmp.y() + (t*r.x()*r.z()-s*r.y())*tmp.z();
	v.y()=(t*r.x()*r.y()-s*r.z())*tmp.x() + (t*r.y()*r.y()+c)*tmp.y() + (t*r.y()*r.z()+s*r.x())*tmp.z();
	v.z()=(t*r.x()*r.z()+s*r.y())*tmp.x() + (t*r.y()*r.z()-s*r.x())*tmp.y() + (t*r.z()*r.z()+c)*tmp.z();

	return(v);
}

template<class T1,class F1,class T2,class F2>
OpenUtility::CMat<T1,3,1,F1>** OpenUtility::Vec3VRotate(CMat<T1,3,1,F1> **v,const unsigned int size,const CMat<T2,3,1,F2> &r,T1 angle)
{
	T1 theta=static_cast<T1>(angle/(180*pi));
	T1 c=cos(theta);
	T1 s=sin(theta);
	T1 t=1-c;
	T1 m[9];

	m[0]=t*r.x()*r.x()+c;
	m[1]=t*r.x()*r.y()+s*r.z();
	m[2]=t*r.x()*r.z()-s*r.y();
	m[3]=t*r.x()*r.y()-s*r.z();
	m[4]=t*r.y()*r.y()+c;
	m[5]=t*r.y()*r.z()+s*r.x();
	m[6]=t*r.x()*r.z()+s*r.y();
	m[7]=t*r.y()*r.z()-s*r.x();
	m[8]=t*r.z()*r.z()+c;

	for (unsigned int i=0;i<size;i++)
	{
		CMat<T1,3,1,F1> tmp=*v[i];

		v[i]->x()=m[0]*tmp.x() + m[1]*tmp.y() + m[2]*tmp.z();
		v[i]->y()=m[3]*tmp.x() + m[4]*tmp.y() + m[5]*tmp.z();
		v[i]->z()=m[6]*tmp.x() + m[7]*tmp.y() + m[8]*tmp.z();
	}

	return(v);
}

template<class T1,class F1,class T2,class F2,class T3,class F3>
void OpenUtility::Vec3ComputeOrthoBase(CMat<T1,3,1,F1> &v1,CMat<T2,3,1,F2> &v2,CMat<T3,3,1,F3> &v3)
{
	v1.Normalize();
	Vec3CrossProd(v1,v2,v3);
	v3.Normalize();
	Vec3CrossProd(v3,v1,v2);
	v2.Normalize();
}

template<class T1,class F1,class T2,class F2,class T3,class F3>
void OpenUtility::Vec3ComputeOrthoBase2ndCst(CMat<T1,3,1,F1> &v1,CMat<T2,3,1,F2> &v2,CMat<T3,3,1,F3> &v3)
{
	v2.Normalize();
	Vec3CrossProd(v1,v2,v3);
	v3.Normalize();
	Vec3CrossProd(v2,v3,v1);
	v1.Normalize();
}

//--------------------------------------------------------------------------------------------------------
// --- Square of dim 4

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::Invert()
{
	T det;
	T inv[4*4];

	inv[0]=Storage(5)*Storage(10)*Storage(15) - Storage(5)*Storage(14)*Storage(11) - Storage(6)*Storage(9)*Storage(15)
		+ Storage(6)*Storage(13)*Storage(11) + Storage(7)*Storage(9)*Storage(14) - Storage(7)*Storage(13)*Storage(10);
	inv[1]=-Storage(1)*Storage(10)*Storage(15) + Storage(1)*Storage(14)*Storage(11) + Storage(2)*Storage(9)*Storage(15)
		- Storage(2)*Storage(13)*Storage(11) - Storage(3)*Storage(9)*Storage(14) + Storage(3)*Storage(13)*Storage(10);
	inv[2]=Storage(1)*Storage(6)*Storage(15) - Storage(1)*Storage(14)*Storage(7) - Storage(2)*Storage(5)*Storage(15)
		+ Storage(2)*Storage(13)*Storage(7) + Storage(3)*Storage(5)*Storage(14) - Storage(3)*Storage(13)*Storage(6);
	inv[3]=-Storage(1)*Storage(6)*Storage(11) + Storage(1)*Storage(10)*Storage(7) + Storage(2)*Storage(5)*Storage(11)
		- Storage(2)*Storage(9)*Storage(7) - Storage(3)*Storage(5)*Storage(10) + Storage(3)*Storage(9)*Storage(6);
	inv[4]=-Storage(4)*Storage(10)*Storage(15) + Storage(4)*Storage(14)*Storage(11) + Storage(6)*Storage(8)*Storage(15)
		- Storage(6)*Storage(12)*Storage(11) - Storage(7)*Storage(8)*Storage(14) + Storage(7)*Storage(12)*Storage(10);
	inv[5]=Storage(0)*Storage(10)*Storage(15) - Storage(0)*Storage(14)*Storage(11) - Storage(2)*Storage(8)*Storage(15)
		+ Storage(2)*Storage(12)*Storage(11) + Storage(3)*Storage(8)*Storage(14) - Storage(3)*Storage(12)*Storage(10);
	inv[6]=-Storage(0)*Storage(6)*Storage(15) + Storage(0)*Storage(14)*Storage(7) + Storage(2)*Storage(4)*Storage(15)
		- Storage(2)*Storage(12)*Storage(7) - Storage(3)*Storage(4)*Storage(14) + Storage(3)*Storage(12)*Storage(6);
	inv[7]=Storage(0)*Storage(6)*Storage(11) - Storage(0)*Storage(10)*Storage(7) - Storage(2)*Storage(4)*Storage(11)
		+ Storage(2)*Storage(8)*Storage(7) + Storage(3)*Storage(4)*Storage(10) - Storage(3)*Storage(8)*Storage(6);
	inv[8]=Storage(4)*Storage(9)*Storage(15) - Storage(4)*Storage(13)*Storage(11) - Storage(5)*Storage(8)*Storage(15)
		+ Storage(5)*Storage(12)*Storage(11) + Storage(7)*Storage(8)*Storage(13) - Storage(7)*Storage(12)*Storage(9);
	inv[9]=-Storage(0)*Storage(9)*Storage(15) + Storage(0)*Storage(13)*Storage(11) + Storage(1)*Storage(8)*Storage(15)
		- Storage(1)*Storage(12)*Storage(11) - Storage(3)*Storage(8)*Storage(13) + Storage(3)*Storage(12)*Storage(9);
	inv[10]=Storage(0)*Storage(5)*Storage(15) - Storage(0)*Storage(13)*Storage(7) - Storage(1)*Storage(4)*Storage(15)
		+ Storage(1)*Storage(12)*Storage(7) + Storage(3)*Storage(4)*Storage(13) - Storage(3)*Storage(12)*Storage(5);
	inv[11]=-Storage(0)*Storage(5)*Storage(11) + Storage(0)*Storage(9)*Storage(7) + Storage(1)*Storage(4)*Storage(11)
		- Storage(1)*Storage(8)*Storage(7) - Storage(3)*Storage(4)*Storage(9) + Storage(3)*Storage(8)*Storage(5);
	inv[12]=-Storage(4)*Storage(9)*Storage(14) + Storage(4)*Storage(13)*Storage(10) + Storage(5)*Storage(8)*Storage(14)
		- Storage(5)*Storage(12)*Storage(10) - Storage(6)*Storage(8)*Storage(13) + Storage(6)*Storage(12)*Storage(9);
	inv[13]=Storage(0)*Storage(9)*Storage(14) - Storage(0)*Storage(13)*Storage(10) - Storage(1)*Storage(8)*Storage(14)
		+ Storage(1)*Storage(12)*Storage(10) + Storage(2)*Storage(8)*Storage(13) - Storage(2)*Storage(12)*Storage(9);
	inv[14]=-Storage(0)*Storage(5)*Storage(14) + Storage(0)*Storage(13)*Storage(6) + Storage(1)*Storage(4)*Storage(14)
		- Storage(1)*Storage(12)*Storage(6) - Storage(2)*Storage(4)*Storage(13) + Storage(2)*Storage(12)*Storage(5);
	inv[15]=Storage(0)*Storage(5)*Storage(10) - Storage(0)*Storage(9)*Storage(6) - Storage(1)*Storage(4)*Storage(10)
		+ Storage(1)*Storage(8)*Storage(6) + Storage(2)*Storage(4)*Storage(9) - Storage(2)*Storage(8)*Storage(5);

	det=Storage(0)*inv[0] + Storage(4)*inv[1] + Storage(8)*inv[2] + Storage(12)*inv[3];
	if (std::abs(det)<std::numeric_limits<T>::epsilon()) return(*this);
	det=1/det;

	Loop<4*4>::Do([&](unsigned int i)
	{
		Storage(i)=inv[i]*det;
	});

	return(*this);
}

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::SetOrtho(T left,T right,T bottom,T top,T zNear,T zFar)
{
	Set(2/(right-left),0,0,-(right+left)/(right-left),
		0,2/(top-bottom),0,-(top+bottom)/(top-bottom),
		0,0,-2/(zFar-zNear),-(zFar+zNear)/(zFar-zNear),
		0,0,0,1);
	return(*this);
}

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::SetFrustum(T left,T right,T bottom,T top,T zNear,T zFar)
{
	Set(2*zNear/(right-left),0,(right+left)/(right-left),0,
		0,2*zNear/(top-bottom),(top+bottom)/(top-bottom),0,
		0,0,-(zFar+zNear)/(zFar-zNear),-(2*zFar*zNear)/(zFar-zNear),
		0,0,-1,0);
	return(*this);
}

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::SetLookAt(T eyeX,T eyeY,T eyeZ,T centerX,T centerY,T centerZ,T upX,T upY,T upZ)
{
	CMat<T,3,1,F> view(centerX-eyeX,centerY-eyeY,centerZ-eyeZ);
	CMat<T,3,1,F> up(upX,upY,upZ);
	CMat<T,3,1,F> cross(MatrixInit::No);

	Vec3ComputeOrthoBase(view,up,cross);
	Set(cross.x(),cross.y(),cross.z(),0,
		up.x(),up.y(),up.z(),0,
		-view.x(),-view.y(),-view.z(),0,
		0,0,0,1);
	MulTranslate(-eyeX,-eyeY,-eyeZ);

	return(*this);
}

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::SetTranslate(T tx,T ty,T tz)
{
	SetIdentity();
	Storage(3)=tx;
	Storage(7)=ty;
	Storage(11)=tz;
	return(*this);
}

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::MulTranslate(T tx,T ty,T tz)
{
	Storage(3)=Storage(0)*tx+Storage(1)*ty+Storage(2)*tz+Storage(3);
	Storage(7)=Storage(4)*tx+Storage(5)*ty+Storage(6)*tz+Storage(7);
	Storage(11)=Storage(8)*tx+Storage(9)*ty+Storage(10)*tz+Storage(11);
	Storage(15)=Storage(12)*tx+Storage(13)*ty+Storage(14)*tz+Storage(15);
	return(*this);
}

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::SetRotate(T angle,T rx,T ry,T rz)
{
	T t;

	if (std::abs((t=rx*rx+ry*ry+rz*rz)-1)>std::numeric_limits<T>::epsilon())
	{
		if (std::abs(t)<std::numeric_limits<T>::epsilon()) return(*this);
		t=static_cast<T>(sqrt(t));
		rx/=t;
		ry/=t;
		rz/=t;
	}

	T theta=static_cast<T>(angle/(180*pi));
	T c=static_cast<T>(cos(theta));
	T s=static_cast<T>(sin(theta));
	t=1-c;

	Set(t*rx*rx+c,t*rx*ry-s*rz,t*rx*rz+s*ry,0,
		t*rx*ry+s*rz,t*ry*ry+c,t*ry*rz-s*rx,0,
		t*rx*rz-s*ry,t*ry*rz+s*rx,t*rz*rz+c,0,
		0,0,0,1);
	return(*this);
}

template<class T,class F>
OpenUtility::CMat<T,4,4,F>& OpenUtility::CMat<T,4,4,F>::SetScale(T sx,T sy,T sz)
{
	SetNull();
	Storage(0)=sx;
	Storage(5)=sy;
	Storage(10)=sz;
	Storage(15)=1;
	return(*this);
}

	#endif
#endif
