//  OpenUtility CMat_Impl.hpp header file

//  (C) Copyright Fabien Picarougne 2014.
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

#ifdef _OU_CMat_h
	#ifndef _OU_CMat_Impl_h
		#define _OU_CMat_Impl_h

#include <cmath>
#include <limits>

//--------------------------------------------------------------------------------------------------------
// Storage policies

template<class T,int R,int C,typename Derived>
template<class T2,class Derived2> Derived& OpenUtility::SStorage<T,R,C,Derived>::copy(const SStorage<T2,R,C,Derived2> &obj)
{
	SStorage<T,R,C,Derived> *pThis=this;

	Loop<R>::Do([&](unsigned int r)
	{
		Loop<C>::Do([&](unsigned int c)
		{
			pThis->Get(r,c)=obj.Get(r,c);
		});
	});

	return(static_cast<Derived&>(*this));
}

template<class T,int R,int C,int H,int W,int R1,int C1,typename Derived1>
void OpenUtility::SViewMatrixStorage<T,R,C,H,W,R1,C1,Derived1>::SetNull()
{
	Loop<R>::Do([&](unsigned int r)
	{
		Loop<C>::Do([&](unsigned int c)
		{
			Storage(R+r,C+c)=0;
		});
	});
}

//--------------------------------------------------------------------------------------------------------
// General operations

template<int R,int C,class F,typename Derived>
OpenUtility::CMat<typename F::value_type,C,R> OpenUtility::Transpose(const CMatBase<R,C,F,Derived> &mat)
{
	CMat<typename F::value_type,C,R> res(MatrixInit::No);

	Loop<R>::Do([&](unsigned int r)
	{
		Loop<C>::Do([&](unsigned int c)
		{
			res.SetVal(c,r,mat(r,c));
		});
	});

	return(res);
}

template<int R,int C,class F1,typename D1,class F2,typename D2,class F>
auto OpenUtility::Operator(const CMatBase<R,C,F1,D1> &mat1,const CMatBase<R,C,F2,D2> &mat2,F func) -> CMat<decltype(func(mat1.GetVal(0),mat2.GetVal(0))),R,C>
{
	CMat<decltype(func(mat1.GetVal(0),mat2.GetVal(0))),R,C> res(MatrixInit::No);

	Loop<R*C>::Do([&](unsigned int i)
	{
		res.GetVal(i)=func(mat1.GetVal(i),mat2.GetVal(i));
	});

	return(res);
}

template<int R1,int N,class F1,typename Derived1,int C2,class F2,typename Derived2>
auto OpenUtility::operator*(const CMatBase<R1,N,F1,Derived1> &mat1,const CMatBase<N,C2,F2,Derived2> &mat2) -> CMat<decltype(mat1(0,0)*mat2(0,0)),R1,C2>
{
	typedef decltype(mat1(0,0)*mat2(0,0)) T;
	CMat<T,R1,C2> res(MatrixInit::No);

	Loop<R1>::Do([&](unsigned int r)
	{
		Loop<C2>::Do([&](unsigned int c)
		{
			T val=0;
			Loop<N>::Do([&](unsigned int i)
			{
				val+=mat1(r,i)*mat2(i,c);
			});
			res.SetVal(r,c,val);
		});
	});

	return(res);
}

template<int N,class F1,typename Derived1,class F2,typename Derived2>
auto OpenUtility::operator*(const CMatBase<1,N,F1,Derived1> &mat1,const CMatBase<N,1,F2,Derived2> &mat2) -> decltype(mat1.GetVal(0)*mat2.GetVal(0))
{
	typedef decltype(mat1.GetVal(0)*mat2.GetVal(0)) T;
	T val=0;

	Loop<N>::Do([&](unsigned int i)
	{
		val+=mat1.GetVal(i)*mat2.GetVal(i);
	});

	return(val);
}

template<class T1,int R,int C,typename Derived1,class T2,typename Derived2>
typename std::enable_if<!std::is_floating_point<T1>::value && !std::is_floating_point<T2>::value,bool>::type OpenUtility::operator==(const SStorage<T1,R,C,Derived1> &obj1,const SStorage<T2,R,C,Derived2> &obj2)
{
	return(Loop<R*C>::DoTest([&](unsigned int i) { return(obj1.Get(i)==obj2.Get(i)); }));
}

template<class T1,int R,int C,typename Derived1,class T2,typename Derived2>
typename std::enable_if<std::is_floating_point<T1>::value,bool>::type OpenUtility::operator==(const SStorage<T1,R,C,Derived1> &obj1,const SStorage<T2,R,C,Derived2> &obj2)
{
	return(Loop<R*C>::DoTest([&](unsigned int i) { return(std::abs(obj1.Get(i)-obj2.Get(i))<std::numeric_limits<T1>::epsilon()); }));
}

//--------------------------------------------------------------------------------------------------------
// Specialisation

// --- Square

template<int R,class F,typename Derived>
Derived& OpenUtility::CMatSquare<R,F,Derived>::SetIdentity()
{
	SetNull();

	Loop<R>::Do([&](unsigned int i)
	{
		Storage(i,i)=1;
	});

	return(static_cast<Derived&>(*this));
}

template<int R,class F,typename Derived>
template<class F2,typename Derived2>
Derived& OpenUtility::CMatSquare<R,F,Derived>::Mul(const CMatSquare<R,F2,Derived2> &m)
{
	typedef typename F::value_type T;
	SDenseStorage<T,R,R> tmp;
	CMatSquare<R,F,Derived> *pThis=this;

	Loop<R>::Do([&](unsigned int r)
	{
		Loop<R>::Do([&](unsigned int c)
		{
			T val=0;
			Loop<R>::Do([&](unsigned int i)
			{
				val+=pThis->Storage(r,i)*m.Storage(i,c);
			});
			tmp(r,c)=val;
		});
	});
	SetMatrix(tmp);

	return(static_cast<Derived&>(*this));
}

template<int R,class F,typename Derived>
Derived& OpenUtility::CMatSquare<R,F,Derived>::Transpose()
{
	typedef typename F::value_type T;

	LoopSquare<0,R>::DoTriangleSup([&](unsigned int r,unsigned int c)
	{
		T val=Storage(r,c);
		Storage(r,c)=Storage(c,r);
		Storage(c,r)=val;
	});

	return(static_cast<Derived&>(*this));
}

	#endif
#endif
