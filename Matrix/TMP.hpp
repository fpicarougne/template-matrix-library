//  OpenUtility TMP.hpp header file

//  (C) Copyright Fabien Picarougne 2014.
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

#ifndef _OU_TMP_h
	#define _OU_TMP_h

namespace OpenUtility
{

template<class... T> void unused(T&&...) {}

template<int I>
class Loop
{
public:
	template<class F>
	static inline void Do(F func)
	{
		Loop<I-1>::Do(func);
		func(I-1);
	}

	template<class F>
	static inline bool DoTest(F func)
	{
		if (Loop<I-1>::DoTest(func)) return(func(I-1));
		return(false);
	}
};

template<>
class Loop<0>
{
public:
	template<class F>
	static inline void Do(F) {}

	template<class F>
	static inline bool DoTest(F) { return(true); }
};

template<int MIN,int MAX>
class LoopSquare
{
	template<int M,int N>
	friend class LoopSquare;

public:
	template<class F>
	static inline void DoTriangle(F func)
	{
		LoopSquare<MIN,MAX-1>::DoTriangle(func);
		LoopSquare<MIN,MAX>::DoTriangle2(func);
	}

	template<class F>
	static inline void DoTriangleSup(F func)
	{
		LoopSquare<MIN,MAX-1>::DoTriangleSup(func);
		LoopSquare<MIN,MAX-1>::DoTriangleSup2(func);
	}

private:
	template<class F>
	static inline void DoTriangle2(F func)
	{
		func(MIN,MAX-1);
		LoopSquare<MIN+1,MAX>::DoTriangle2(func);
	}

	template<class F>
	static inline void DoTriangleSup2(F func)
	{
		func(MIN,MAX);
		LoopSquare<MIN+1,MAX>::DoTriangleSup2(func);
	}
};

template<int MIN>
class LoopSquare<MIN,MIN>
{
	template<int M,int N>
	friend class LoopSquare;

public:
	template<class F> static inline void DoTriangle(F) {}
	template<class F> static inline void DoTriangleSup(F) {}

private:
	template<class F> static inline void DoTriangle2(F) {}
	template<class F> static inline void DoTriangleSup2(F) {}
};

//--------------------------------------------------------------------------------------------------------

template<class T,class N>
struct TypeList
{
	typedef T type;
	typedef N next;
};

template<template<typename> class G,typename... A>
struct GenTypeListGenerator;

template<template<typename> class G,class F,typename... A>
struct GenTypeListGenerator<G,F,A...>
{
	typedef TypeList<typename G<F>::type,typename GenTypeListGenerator<G,A...>::next> next;
};

template<template<typename> class G>
struct GenTypeListGenerator<G>
{
	typedef void next;
};

template<class T>
struct DefGenerator
{
	typedef T type;
};

template<typename... A>
using GenTypeList=GenTypeListGenerator<DefGenerator,A...>;


template<template<int> class G,int... I>
struct GenIntegralListGenerator;

template<template<int> class G,int N,int... I>
struct GenIntegralListGenerator<G,N,I...>
{
	typedef TypeList<typename G<N>::type,typename GenIntegralListGenerator<G,I...>::next> next;
};

template<template<int> class G>
struct GenIntegralListGenerator<G>
{
	typedef void next;
};

//--------------------------------------------------------------------------------------------------------

template<class G,int N=0>
struct ListCount
{
	static const int value=ListCount<typename G::next,N+1>::value;
};

template<>
struct ListCount<void>
{
	static const int value=0;
};

template<int N>
struct ListCount<void,N>
{
	static const int value=N-1;
};

//--------------------------------------------------------------------------------------------------------

template<class T>
struct is_carray
{
	static const bool value=false;
	static const bool staticvalue=false;
	static const int dim=0;
	typedef void valuetype;
};

template <class T,int R>
struct is_carray<T[R]>
{
	static const bool value=true;
	static const bool staticvalue=true;
	static const int dim=R;
	typedef T valuetype;
};

template <class T>
struct is_carray<T*>
{
	static const bool value=true;
	static const bool staticvalue=false;
	static const int dim=0;
	typedef T valuetype;
};

}

#endif
