//  OpenUtility CMat3D.hpp header file

//  (C) Copyright Fabien Picarougne 2014.
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

#ifndef _OU_CMat3D_h
	#define _OU_CMat3D_h

#include "CMat.hpp"

namespace OpenUtility
{

//--------------------------------------------------------------------------------------------------------
// Specialisation

// 4x4 matrix with geometric operations

template<class T,class F>
class CMat<T,4,4,F> : public CMatSquare<4,F,CMat<T,4,4,F>>
{
private:
	typedef CMatSquare<4,F,CMat<T,4,4,F>> Base;

public:
	using Base::Storage;
	using Base::Set;
	using Base::SetIdentity;
	using Base::SetNull;

	CMat(const F &storage,MatrixInit init=MatrixInit::Null) : Base(storage,init) {}
	CMat(MatrixInit init=MatrixInit::Null) : Base(init) {}
	CMat(const F &storage,MatrixSquareInit init) : Base(storage,init) {}
	CMat(MatrixSquareInit init) : Base(init) {}
	template<typename... A> explicit CMat(const F &storage,T val,A... arg) : Base(storage,val,arg...) {}
	template<typename... A> explicit CMat(T val,A... arg) : Base(val,arg...) {}
	CMat(const CMat &obj) : Base(obj) {}
	template<class F2> CMat(const CMat<T,4,4,F2> &obj) : Base(obj) {}
	CMat& operator=(const CMat &obj) { Base::operator=(obj);return(*this); }

	// Math operations
	CMat<T,4,4,F>& Invert();

	// 3D operations
	CMat<T,4,4,F>& SetOrtho(T left,T right,T bottom,T top,T nearVal,T farVal);
	CMat<T,4,4,F>& SetFrustum(T left,T right,T bottom,T top,T nearVal,T farVal);
	CMat<T,4,4,F>& SetLookAt(T eyeX,T eyeY,T eyeZ,T centerX,T centerY,T centerZ,T upX,T upY,T upZ);
	CMat<T,4,4,F>& SetTranslate(T tx,T ty,T tz);
	CMat<T,4,4,F>& MulTranslate(T tx,T ty,T tz);
	CMat<T,4,4,F>& SetRotate(T angle,T rx,T ry,T rz);
	inline CMat<T,4,4,F>& MulRotate(T angle,T rx,T ry,T rz) { return(Mul(CMat<T,4,4,F>(false).SetRotate(angle,rx,ry,rz))); }
	CMat<T,4,4,F>& SetScale(T sx,T sy,T sz);
	inline CMat<T,4,4,F>& MulScale(T sx,T sy,T sz) { return(Mul(CMat<T,4,4,F>(false).SetScale(sx,sy,sz))); }
};

template<class T,class F=SDenseStorage<T,4,4>>
using CMat4x4=CMat<T,4,4,F>;

// Vector of dim 4

template<class T,class F=SDenseStorage<T,4,1>>
using CVec4=CMat<T,4,1,F>;

// Vector of dim 3

template<class T,class F=SDenseStorage<T,3,1>>
using CVec3=CMat<T,3,1,F>;

//--------------------------------------------------------------------------------------------------------
// General operations

template<class T1,class F1,class T2,class F2,class T3,class F3>
CMat<T3,3,1,F3>& Vec3CrossProd(const CMat<T1,3,1,F1> &v,const CMat<T2,3,1,F2> &w,CMat<T3,3,1,F3> &r);

template<class T1,class F1,class T2,class F2>
CMat<T1,3,1,F1>& Vec3Rotate(CMat<T1,3,1,F1> &v,const CMat<T2,3,1,F2> &r,const T1 angle);

template<class T1,class F1,class T2,class F2>
CMat<T1,3,1,F1>** Vec3VRotate(CMat<T1,3,1,F1> **v,const unsigned int size,const CMat<T2,3,1,F2> &r,const T1 angle);

template<class T1,class F1,class T2,class F2,class T3,class F3>
void Vec3ComputeOrthoBase(CMat<T1,3,1,F1> &v1,CMat<T2,3,1,F2> &v2,CMat<T3,3,1,F3> &v3);

template<class T1,class F1,class T2,class F2,class T3,class F3>
void Vec3ComputeOrthoBase2ndCst(CMat<T1,3,1,F1> &v1,CMat<T2,3,1,F2> &v2,CMat<T3,3,1,F3> &v3);

}

#include "Impl/CMat3D_Impl.hpp"

#endif
