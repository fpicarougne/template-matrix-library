//  OpenUtility CMat.hpp header file

//  (C) Copyright Fabien Picarougne 2014.
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

#ifndef _OU_CMat_h
	#define _OU_CMat_h

#include "TMP.hpp"
#include <string.h>
#include <iostream>
#include <type_traits>

#ifdef GEN_SWIZZLE_FUNCTION
#include <boost/preprocessor/seq/for_each_product.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/cat.hpp>
#include <boost/preprocessor/seq/for_each_i.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>
#endif

namespace OpenUtility
{

//--------------------------------------------------------------------------------------------------------
// Storage policies

template<class T,int R,int C,typename Derived>
struct SStorage
{
	typedef T value_type;

	template<class T2,class Derived2> Derived& copy(const SStorage<T2,R,C,Derived2> &obj);
	template<class T2,class Derived2> inline Derived& operator=(const SStorage<T2,R,C,Derived2> &obj) { return(copy(obj)); }
	inline SStorage& operator=(const SStorage &obj) { return(static_cast<Derived*>(this)->operator=(static_cast<const Derived&>(obj))); }
	void SetNull() { static_cast<Derived*>(this)->SetNull(); }
	inline T Get(unsigned int r,unsigned int c) const { return(static_cast<const Derived*>(this)->Get(r,c)); }
	inline T& Get(unsigned int r,unsigned int c) { return(static_cast<Derived*>(this)->Get(r,c)); }
	template<int R1,int C1> inline T Get() const { return(static_cast<const Derived*>(this)->template Get<R1,C1>()); }
	template<int R1,int C1> inline T& Get() { return(static_cast<Derived*>(this)->template Get<R1,C1>()); }
	inline T operator()(unsigned int r,unsigned int c) const { return(Get(r,c)); }
	inline T& operator()(unsigned int r,unsigned int c) { return(Get(r,c)); }
	// Row order
	inline T Get(unsigned int i) const { return(static_cast<const Derived*>(this)->Get(i)); }
	inline T& Get(unsigned int i) { return(static_cast<Derived*>(this)->Get(i)); }
	template<int I> inline T Get() const { return(static_cast<const Derived*>(this)->template Get<I>()); }
	template<int I> inline T& Get() { return(static_cast<Derived*>(this)->template Get<I>()); }
	inline T operator()(unsigned int i) const { return(Get(i)); }
	inline T& operator()(unsigned int i) { return(Get(i)); }
};

template<class T1,int R1,int C1,typename Derived1,class T2,int R2,int C2,typename Derived2>
inline typename std::enable_if<R1!=R2 || C1!=C2,bool>::type operator==(const SStorage<T1,R1,C1,Derived1> &obj1,const SStorage<T2,R2,C2,Derived2> &obj2) { return(false); }
template<class T1,int R,int C,typename Derived1,class T2,typename Derived2>
typename std::enable_if<!std::is_floating_point<T1>::value && !std::is_floating_point<T2>::value,bool>::type operator==(const SStorage<T1,R,C,Derived1> &obj1,const SStorage<T2,R,C,Derived2> &obj2);
template<class T1,int R,int C,typename Derived1,class T2,typename Derived2>
typename std::enable_if<std::is_floating_point<T1>::value,bool>::type operator==(const SStorage<T1,R,C,Derived1> &obj1,const SStorage<T2,R,C,Derived2> &obj2);
template<class T1,int R,int C,typename Derived1,class T2,typename Derived2>
inline typename std::enable_if<!std::is_floating_point<T1>::value && std::is_floating_point<T2>::value,bool>::type operator==(const SStorage<T1,R,C,Derived1> &obj1,const SStorage<T2,R,C,Derived2> &obj2) { return(obj2==obj1); }
template<class T1,int R1,int C1,typename Derived1,class T2,int R2,int C2,typename Derived2>
inline typename std::enable_if<R1!=R2 || C1!=C2,bool>::type operator!=(const SStorage<T1,R1,C1,Derived1> &obj1,const SStorage<T2,R2,C2,Derived2> &obj2) { return(!(obj1==obj2)); }

template<int R,int C>
struct SRowMajor
{
//TODO: use a constexpr
	template<int I> unsigned int Get() const { return(I); }
	inline unsigned int operator()(unsigned int i) const { return(i); }
	template<int R1,int C1> unsigned int Get() const { return(C1+R1*C); }
	inline unsigned int operator()(unsigned int r,unsigned int c) const { return(c+r*C); }
};

template<int R,int C>
struct SColumnMajor
{
	//TODO: use a constexpr
	template<int I> unsigned int Get() const { return((I%C)*R+(I/C)); }
	inline unsigned int operator()(unsigned int i) const { return((i%C)*R+(i/C)); }
	template<int R1,int C1> unsigned int Get() const { return(R1+C1*R); }
	inline unsigned int operator()(unsigned int r,unsigned int c) const { return(r+c*R); }
};

template<class T,int R,int C,template<int,int> class F=SColumnMajor>
struct SDenseStorage : public SStorage<T,R,C,SDenseStorage<T,R,C,F>>
{
	template<class T2,int R2,int C2,template<int,int> class F2>
	friend struct SDenseStorage;

	typedef T* type;

	SDenseStorage() {}
	SDenseStorage(const SDenseStorage &obj) { memcpy(Mat,obj.Mat,sizeof(Mat)); }
	template<template<int,int> class F2> inline SDenseStorage(const SDenseStorage<T,R,C,F2> &obj) { memcpy(Mat,obj.Mat,sizeof(Mat)); }
	inline SDenseStorage& operator=(const SDenseStorage &obj) { memcpy(Mat,obj.Mat,sizeof(Mat));return(*this); }
	template<class T2,class Derived2> inline SDenseStorage& operator=(const SStorage<T2,R,C,Derived2> &obj) { return(Base::operator=(obj)); }
	inline type GetMatrix() { return(Mat); }
	inline void SetNull() { memset(Mat,0,sizeof(Mat)); }
	inline T Get(unsigned int r,unsigned int c) const { return(Mat[Access(r,c)]); }
	inline T& Get(unsigned int r,unsigned int c) { return(Mat[Access(r,c)]); }
	template<int R1,int C1> inline T Get() const { return(Mat[Access.template Get<R1,C1>()]); }
	template<int R1,int C1> inline T& Get() { return(Mat[Access.template Get<R1,C1>()]); }
	inline T Get(unsigned int i) const { return(Mat[Access(i)]); }
	inline T& Get(unsigned int i) { return(Mat[Access(i)]); }
	template<int I> inline T Get() const { return(Mat[Access.template Get<I>()]); }
	template<int I> inline T& Get() { return(Mat[Access.template Get<I>()]); }

protected:
	T Mat[R*C];
	F<R,C> Access;

private:
	typedef SStorage<T,R,C,SDenseStorage<T,R,C,F>> Base;
};

template<class T,int R,int C,int H,int W,int R1,int C1,typename Derived1>
struct SViewMatrixStorage : public SStorage<T,H,W,SViewMatrixStorage<T,R,C,H,W,R1,C1,Derived1>>
{
	SViewMatrixStorage(SStorage<T,R1,C1,Derived1> &obj) : Storage(obj) {}
	inline SViewMatrixStorage& operator=(const SViewMatrixStorage &obj) { return(copy(obj)); }
	template<class T2,class Derived2> inline SViewMatrixStorage& operator=(const SStorage<T2,H,W,Derived2> &obj) { return(copy(obj)); }
	void SetNull();
	inline T Get(unsigned int r,unsigned int c) const { return(Storage(R+r,C+c)); }
	inline T& Get(unsigned int r,unsigned int c) { return(Storage(R+r,C+c)); }
	template<int R2,int C2> inline T Get() const { return(Storage.template Get<R+R2,C+C2>()); }
	template<int R2,int C2> inline T& Get() { return(Storage.template Get<R+R2,C+C2>()); }
	// Row order
	inline T Get(unsigned int i) const { return(Storage(R+i/W,C+i%W)); }
	inline T& Get(unsigned int i) { return(Storage(R+i/W,C+i%W)); }
	template<int I> inline T Get() const { return(Storage.template Get<R+I/W,C+I%W>()); }
	template<int I> inline T& Get() { return(Storage.template Get<R+I/W,C+I%W>()); }

protected:
	SStorage<T,R1,C1,Derived1> &Storage;

private:
	typedef SStorage<T,H,W,SViewMatrixStorage<T,R,C,H,W,R1,C1,Derived1>> Base;
	using Base::copy;
};

template<class T,int N,typename Derived1>
struct SViewDiagonalStorage : public SStorage<T,N,1,SViewDiagonalStorage<T,N,Derived1>>
{
	SViewDiagonalStorage(SStorage<T,N,N,Derived1> &obj) : Storage(obj) {}
	inline SViewDiagonalStorage& operator=(const SViewDiagonalStorage &obj) { return(copy(obj)); }
	template<class T2,class Derived2> inline SViewDiagonalStorage& operator=(const SStorage<T2,N,1,Derived2> &obj) { return(copy(obj)); }
	void SetNull() { Loop<N>::Do([&](const unsigned int i) { Get(i)=0; }); }
	inline T Get(unsigned int r,unsigned int c) const { unused(c);return(Storage(r,r)); }
	inline T& Get(unsigned int r,unsigned int c) { unused(c);return(Storage(r,r)); }
	template<int R1,int C1> inline T Get() const { return(Storage.template Get<R1,R1>()); }
	template<int R1,int C1> inline T& Get() { return(Storage.template Get<R1,R1>()); }
	// Row order
	inline T Get(unsigned int i) const { return(Storage(i,i)); }
	inline T& Get(unsigned int i) { return(Storage(i,i)); }
	template<int I> inline T Get() const { return(Storage.template Get<I,I>()); }
	template<int I> inline T& Get() { return(Storage.template Get<I,I>()); }

protected:
	SStorage<T,N,N,Derived1> &Storage;

private:
	typedef SStorage<T,N,1,SViewDiagonalStorage<T,N,Derived1>> Base;
	using Base::copy;
};

template<class T,int R,int C>
struct SProxyArrayStorage : public SStorage<T,R,C,SProxyArrayStorage<T,R,C>>
{
	typedef T* type;

	SProxyArrayStorage(T(&tab)[R][C]) : Mat(tab) {}
	inline SProxyArrayStorage& operator=(const SProxyArrayStorage &obj) { memcpy(Mat,obj.m,sizeof(Mat));return(*this); }
	template<class T2,class Derived2> inline SProxyArrayStorage& operator=(const SStorage<T2,R,C,Derived2> &obj) { return(Base::copy(obj)); }
	inline type GetMatrix() { return(reinterpret_cast<T*>(Mat)); }
	inline void SetNull() { memset(Mat,0,sizeof(Mat)); }
	inline T Get(unsigned int r,unsigned int c) const { return(Mat[r][c]); }
	inline T& Get(unsigned int r,unsigned int c) { return(Mat[r][c]); }
	template<int R1,int C1> inline T Get() const { return(Mat[R1][C1]); }
	template<int R1,int C1> inline T& Get() { return(Mat[R1][C1]); }
	// Row order
	inline T Get(unsigned int i) const { return(Mat[i/C][i%C]); }
	inline T& Get(unsigned int i) { return(Mat[i/C][i%C]); }
	template<int I> inline T Get() const { return(Mat[I/C][I%C]); }
	template<int I> inline T& Get() { return(Mat[I/C][I%C]); }

protected:
	T(&Mat)[R][C];

private:
	typedef SStorage<T,R,C,SProxyArrayStorage<T,R,C>> Base;
};

template<class T,int R,int C>
struct SProxyColumnStorage : public SStorage<T,R,C,SProxyColumnStorage<T,R,C>>
{
	typedef T* type;

	SProxyColumnStorage(T *tab) : Mat(tab) {}
	inline SProxyColumnStorage& operator=(const SProxyColumnStorage &obj) { memcpy(Mat,obj.m,sizeof(Mat));return(*this); }
	template<class T2,class Derived2> inline SProxyColumnStorage& operator=(const SStorage<T2,R,C,Derived2> &obj) { return(Base::copy(obj)); }
	inline type GetMatrix() { return(reinterpret_cast<T*>(Mat)); }
	inline void SetNull() { memset(Mat,0,R*C*sizeof(T)); }
	inline T Get(unsigned int r,unsigned int c) const { return(Mat[r*C+c]); }
	inline T& Get(unsigned int r,unsigned int c) { return(Mat[r*C+c]); }
	template<int R1,int C1> inline T Get() const { return(Mat[R1*C+C1]); }
	template<int R1,int C1> inline T& Get() { return(Mat[R1*C+C1]); }
	// Row order
	inline T Get(unsigned int i) const { return(Mat[i]); }
	inline T& Get(unsigned int i) { return(Mat[i]); }
	template<int I> inline T Get() const { return(Mat[I]); }
	template<int I> inline T& Get() { return(Mat[I]); }

protected:
	T *Mat;

private:
	typedef SStorage<T,R,C,SProxyColumnStorage<T,R,C>> Base;
};

template<class T,int R,int C,typename Derived1>
struct STransposeStorage : public SStorage<T,R,C,STransposeStorage<T,R,C,Derived1>>
{
	STransposeStorage(SStorage<T,C,R,Derived1> &obj) : Storage(obj) {}
	inline STransposeStorage& operator=(const STransposeStorage &obj) { Storage=obj.Storage;return(*this); }
	template<class T2,class Derived2> inline STransposeStorage& operator=(const SStorage<T2,R,C,Derived2> &obj) { Storage=obj;return(*this); }
	inline void SetNull() { Storage.SetNull(); }
	inline T Get(unsigned int r,unsigned int c) const { return(Storage(c,r)); }
	inline T& Get(unsigned int r,unsigned int c) { return(Storage(c,r)); }
	template<int R1,int C1> inline T Get() const { return(Storage.template Get<R1,C1>()); }
	template<int R1,int C1> inline T& Get() { return(Storage.template Get<R1,C1>()); }
	// Row order
	inline T Get(unsigned int i) const { return(Storage(i%C,i/C)); }
	inline T& Get(unsigned int i) { return(Storage(i%C,i/C)); }
	template<int I> inline T Get() const { return(Storage.template Get<I%C,I/C>()); }
	template<int I> inline T& Get() { return(Storage.template Get<I%C,I/C>()); }

protected:
	SStorage<T,C,R,Derived1> &Storage;
};

template<class T,int R,int R1,typename Derived1,typename List>
struct SSwizzleStorage : public SStorage<T,R,1,SSwizzleStorage<T,R,R1,Derived1,List>>
{
	explicit SSwizzleStorage(SStorage<T,R1,1,Derived1> &obj) { _Set<0,typename List::next>(obj); }
	inline SSwizzleStorage& operator=(const SSwizzleStorage &obj) { Storage=obj.Storage;return(*this); }
	template<class T2,class Derived2> inline SSwizzleStorage& operator=(const SStorage<T2,R,1,Derived2> &obj) { Storage=obj;return(*this); }
	void SetNull() { Loop<R>::Do([&](unsigned int i) { if ((Storage[i]!=&Zero) && (Storage[i]!=&One)) *Storage[i]=0; }); }
	inline T Get(unsigned int r,unsigned int c) const { unused(c);return(*Storage[r]); }
	inline T& Get(unsigned int r,unsigned int c) { unused(c);return(*Storage[r]); }
	template<int R2,int C2> inline T Get() const { return(*Storage[R2]); }
	template<int R2,int C2> inline T& Get() { return(*Storage[R2]); }
	// Row order
	inline T Get(unsigned int i) const { return(*Storage[i]); }
	inline T& Get(unsigned int i) { return(*Storage[i]); }
	template<int I> inline T Get() const { return(*Storage[I]); }
	template<int I> inline T& Get() { return(*Storage[I]); }

protected:
	template<typename TT> struct type {};
	template<int N,typename List1> inline void _Set(SStorage<T,R1,1,Derived1> &obj) { _Set<N>(type<List1>(),obj); }
	template<int N,typename List1> inline void _Set(type<List1>,SStorage<T,R1,1,Derived1> &obj) { __Set<typename List1::type::type,typename List1::type>(obj,N,List1::type::value);_Set<N+1,typename List1::next>(obj); }
	template<int N> inline void _Set(type<void>,SStorage<T,R1,1,Derived1>&) { static_assert(N>=R,"Too few arguments (");static_assert(N<=R,"Too many arguments"); }
	template<class T1,class T2> inline void __Set(SStorage<T,R1,1,Derived1> &obj,unsigned int i,T1 val) { __Set<T2>(type<T1>(),obj,i,val); }
	template<class T2> inline void __Set(type<unsigned int>,SStorage<T,R1,1,Derived1> &obj,unsigned int i,unsigned int val) { static_assert(T2::value<R1,"Swizzle dimension greater than initial vector dimension");Storage[i]=&obj(val); }
	template<class T2> inline void __Set(type<bool>,SStorage<T,R1,1,Derived1> &obj,unsigned int i,bool val) { unused(obj);val ? Storage[i]=const_cast<T*>(&One) : Storage[i]=const_cast<T*>(&Zero); }

protected:
	static const T Zero;
	static const T One;
	T *Storage[ListCount<List>::value];
};

template<class T,int R,int R1,typename Derived1,typename List>
const T SSwizzleStorage<T,R,R1,Derived1,List>::Zero(0);

template<class T,int R,int R1,typename Derived1,typename List>
const T SSwizzleStorage<T,R,R1,Derived1,List>::One(1);

//--------------------------------------------------------------------------------------------------------
// General matrix class

enum class MatrixInit { No, Null };

template<class T,int R,int C,class F> class CMat;
struct MView;

template<int R,int C,class F,typename Derived>
class CMatBase
{
	template<int R1, int C1,class F1,typename Derived1>
	friend class CMatBase;
	template<int R1,int C1,class F1,typename Derived1,class F2,typename Derived2>
	friend bool operator==(const CMatBase<R1,C1,F1,Derived1> &obj1,const CMatBase<R1,C1,F2,Derived2> &obj2);
	friend struct MView;

public:
	CMatBase(const F &storage,MatrixInit init=MatrixInit::Null) : Storage(storage) { if (init==MatrixInit::Null) Storage.SetNull(); }
	CMatBase(MatrixInit init=MatrixInit::Null) { if (init==MatrixInit::Null) Storage.SetNull(); }
	// Argument order as in row order: r1c1, r1c2... r2c1, r2c2...
	template<typename... A> explicit CMatBase(const F &storage,typename F::value_type val,A... arg) : Storage(storage) { _Set<0>(val,arg...); }
	template<typename... A> explicit CMatBase(typename F::value_type val,A... arg) { _Set<0>(val,arg...); }
	template<class F2,typename Derived2> CMatBase(const CMatBase<R,C,F2,Derived2> &obj) : Storage(obj.Storage) {}
	Derived& operator=(const CMatBase &obj) { Storage=obj.Storage;return(static_cast<Derived&>(*this)); }
	~CMatBase() {}
	template<class T,typename Derived2> inline Derived& SetMatrix(const SStorage<T,R,C,Derived2> &obj) { Storage=static_cast<const Derived2&>(obj);return(static_cast<Derived&>(*this)); }
	template<class F1=F> typename F1::type GetMatrix() { return(Storage.GetMatrix()); }
	inline Derived& SetNull() { Storage.SetNull();return(static_cast<Derived&>(*this)); }
	inline typename F::value_type& GetVal(unsigned int r,unsigned int c) { return(Storage(r,c)); }
	inline typename F::value_type& GetVal(unsigned int i) { return(Storage(i)); }
	template<int I> inline typename F::value_type& GetVal() { return(Storage.template Get<I>()); }
	inline const typename F::value_type GetVal(unsigned int r,unsigned int c) const { return(Storage(r,c)); }
	inline const typename F::value_type GetVal(unsigned int i) const { return(Storage(i)); }
	template<int R1,int C1> inline typename F::value_type& GetVal() { return(Storage.template Get<R1,C1>()); }
	inline typename F::value_type& operator()(unsigned int r,unsigned int c) { return(GetVal(r,c)); }
	inline typename F::value_type& operator()(unsigned int i) { return(GetVal(i)); }
	inline const typename F::value_type operator()(unsigned int r,unsigned int c) const { return(GetVal(r,c)); }
	inline const typename F::value_type operator()(unsigned int i) const { return(GetVal(i)); }
	inline Derived& SetVal(unsigned int i,typename F::value_type val) { Storage(i)=val;return(static_cast<Derived&>(*this)); }
	template<int I> inline Derived& SetVal(typename F::value_type val) { Storage.template Get<I>()=val;return(static_cast<Derived&>(*this)); }
	inline Derived& SetVal(unsigned int r,unsigned int c,typename F::value_type val) { Storage(r,c)=val;return(static_cast<Derived&>(*this)); }
	template<int R1,int C1> inline Derived& SetVal(typename F::value_type val) { Storage.template Get<R1,C1>()=val;return(static_cast<Derived&>(*this)); }
	// Argument order as in row order: r1c1, r1c2... r2c1, r2c2...
	template<typename... A> Derived& Set(A... arg) { static_assert(sizeof...(A)==R*C,"Wrong number of parameter set.");_Set<0>(arg...);return(static_cast<Derived&>(*this)); }

	// Math operators
	inline Derived& Clamp(typename F::value_type valMin=0,typename F::value_type valMax=1) { Loop<R*C>::Do([&](int i){ if (Storage(i)<valMin) Storage(i)=valMin;if (Storage(i)>valMax) Storage(i)=valMax; });return(static_cast<const Derived&>(*this)); }
	template<class F1,typename D1> inline Derived& Add(const CMatBase<R,C,F1,D1> &m) { Loop<R*C>::Do([&](unsigned int i){ Storage(i)+=m.Storage(i); });static_cast<Derived&>(*this); }
	template<class F1,typename D1> inline Derived& operator+=(const CMatBase<R,C,F1,D1> &m) { return(Add(m)); }
	template<class F1,typename D1> inline Derived& Sub(const CMatBase<R,C,F1,D1> &m) { Loop<R*C>::Do([&](unsigned int i){ Storage(i)-=m.Storage(i); });static_cast<Derived&>(*this); }
	template<class F1,typename D1> inline Derived& operator-=(const CMatBase<R,C,F1,D1> &m) { return(Sub(m)); }
	template<class T1> inline Derived& Add(const T1 c) { Loop<R*C>::Do([&](unsigned int i){ Storage(i)+=c; });return(static_cast<Derived&>(*this)); }
	template<class T1> inline Derived& operator+=(const T1 c) { return(Add(c)); }
	template<class T1> inline Derived& Sub(const T1 c) { Loop<R*C>::Do([&](unsigned int i){ Storage(i)-=c; });return(static_cast<Derived&>(*this)); }
	template<class T1> inline Derived& operator-=(const T1 c) { return(Sub(c)); }
	template<class T1> inline Derived& Mul(const T1 c) { Loop<R*C>::Do([&](unsigned int i){ Storage(i)*=c; });return(static_cast<Derived&>(*this)); }
	template<class T1> inline Derived& operator*=(const T1 c) { return(Mul(c)); }
	template<class T1> inline Derived& Div(const T1 c) { Loop<R*C>::Do([&](unsigned int i){ Storage(i)/=c; });return(static_cast<Derived&>(*this)); }
	template<class T1> inline Derived& operator/=(const T1 c) { return(Div(c)); }

	// Display informations
	friend inline std::ostream& operator<<(std::ostream &o,const CMatBase<R,C,F,Derived> &obj)
	{
		Loop<R>::Do([&](unsigned int r)
		{
			if (r) o << std::endl;
			o << '[';
			Loop<C>::Do([&](unsigned int c)
			{
				if (c) o << ',';
				o << ' ' << obj.Storage(r,c);
			});
			o << " ]";
		});
		return(o);
	}

protected:
	template<int N,typename ...A> void _Set(typename F::value_type argHead,A... argTail) { __Set(N,argHead);_Set<N+1>(argTail...); }
	template<int N> void _Set(typename F::value_type arg) { static_assert(N+1>=R*C,"Too few arguments");static_assert(N+1<=R*C,"Too many arguments");__Set(N,arg); }
	inline void __Set(unsigned int i,typename F::value_type val) { Storage(i)=val; }

protected:
	F Storage;
};

//--------------------------------------------------------------------------------------------------------
// Specialisation for square matrix

enum class MatrixSquareInit { No,Null,Identity };

template<int R,class F,typename Derived>
class CMatSquare : public CMatBase<R,R,F,CMatSquare<R,F,Derived>>
{
private:
	typedef CMatBase<R,R,F,CMatSquare<R,F,Derived>> Base;

public:
	using Base::Storage;
	using Base::SetNull;
	using Base::SetMatrix;

	CMatSquare(const F &storage,MatrixInit init=MatrixInit::Null) : Base(storage,init) {}
	CMatSquare(MatrixInit init=MatrixInit::Null) : Base(init) {}
	CMatSquare(const F &storage,MatrixSquareInit init) : Base(storage,init==MatrixSquareInit::Null ? MatrixInit::Null : MatrixInit::No) { if (init==MatrixSquareInit::Identity) SetIdentity(); }
	CMatSquare(MatrixSquareInit init) : Base(init==MatrixSquareInit::Null ? MatrixInit::Null : MatrixInit::No) { if (init==MatrixSquareInit::Identity) SetIdentity(); }
	template<typename... A> explicit CMatSquare(const F &storage,typename F::value_type val,A... arg) : Base(storage,val,arg...) {}
	template<typename... A> explicit CMatSquare(typename F::value_type val,A... arg) : Base(val,arg...) {}
	CMatSquare(const CMatSquare &obj) : Base(obj) {}
	Derived& operator=(const CMatSquare &obj) { Base::operator=(obj);return(static_cast<Derived&>(*this)); }
	Derived& SetIdentity();

	// Math operations
	template<class F2,typename Derived2> Derived& Mul(const CMatSquare<R,F2,Derived2> &m);
	template<class F2,typename Derived2> inline Derived& operator*=(const CMatSquare<R,F2,Derived2> &m) { return(Mul(m)); }
	Derived& Transpose();
};

//--------------------------------------------------------------------------------------------------------
// Specialisation for vectors

// Swizzle structures

struct _X_;struct _Y_;struct _Z_;struct _W_;struct _R_;struct _G_;struct _B_;struct _A_;struct _x_;struct _y_;struct _z_;struct _w_;struct _r_;struct _g_;struct _b_;struct _a_;struct _0_;struct _1_;

template<class T> struct SVectorDim;
template<> struct SVectorDim<_X_> { typedef unsigned int type;static const int value=0; };
template<> struct SVectorDim<_x_> : public SVectorDim<_X_> {};
template<> struct SVectorDim<_R_> : public SVectorDim<_X_> {};
template<> struct SVectorDim<_r_> : public SVectorDim<_X_> {};
template<> struct SVectorDim<_Y_> { typedef unsigned int type;static const int value=1; };
template<> struct SVectorDim<_y_> : public SVectorDim<_Y_> {};
template<> struct SVectorDim<_G_> : public SVectorDim<_Y_> {};
template<> struct SVectorDim<_g_> : public SVectorDim<_Y_> {};
template<> struct SVectorDim<_Z_> { typedef unsigned int type;static const int value=2; };
template<> struct SVectorDim<_z_> : public SVectorDim<_Z_> {};
template<> struct SVectorDim<_B_> : public SVectorDim<_Z_> {};
template<> struct SVectorDim<_b_> : public SVectorDim<_Z_> {};
template<> struct SVectorDim<_W_> { typedef unsigned int type;static const int value=3; };
template<> struct SVectorDim<_w_> : public SVectorDim<_W_> {};
template<> struct SVectorDim<_A_> : public SVectorDim<_W_> {};
template<> struct SVectorDim<_a_> : public SVectorDim<_W_> {};
template<> struct SVectorDim<_0_> { typedef bool type;static const bool value=false; };
template<> struct SVectorDim<_1_> { typedef bool type;static const bool value=true; };
template<int I> struct SVectorDimI { typedef unsigned int type;static const int value=I; };

// Vector base matrix

#define _BOOST_CMAT_GEN_FUNC_1(pattern,type_modifier,func_modifier) \
	template<typename... Dummy,int U=R> \
	inline typename std::enable_if<(SVectorDim<_##pattern##_>::value<U) ,T type_modifier>::type pattern() func_modifier \
	{ \
		static_assert(sizeof...(Dummy)==0,"Do not specify template arguments!"); \
		return(static_cast<func_modifier Derived*>(this)->Storage(SVectorDim<_##pattern##_>::value)); \
	}
#define _BOOST_CMAT_GEN_FUNC(pattern) _BOOST_CMAT_GEN_FUNC_1(pattern,&,) _BOOST_CMAT_GEN_FUNC_1(pattern,,const)

template<class T,int R,typename Derived>
class CVecBase
{
public:
	inline T& operator[](unsigned int i) { return(static_cast<Derived*>(this)->Storage(i)); }
	inline const T& operator[](unsigned int i) const { return(static_cast<const Derived*>(this)->Storage(i)); }
	inline T ModuleSquare() { T val=0;Loop<R>::Do([&](unsigned int i){ val+=static_cast<Derived*>(this)->Storage(i)*static_cast<Derived*>(this)->Storage(i); });return(val); }
	inline T Module() { T val=ModuleSquare();return(val>0 ? sqrt(val) : 0); }
	inline Derived& Normalize() { T norm=Module();if (norm) static_cast<Derived*>(this)->Div(norm);return(static_cast<Derived&>(*this)); }

	_BOOST_CMAT_GEN_FUNC(x)
	_BOOST_CMAT_GEN_FUNC(y)
	_BOOST_CMAT_GEN_FUNC(z)
	_BOOST_CMAT_GEN_FUNC(w)
	_BOOST_CMAT_GEN_FUNC(r)
	_BOOST_CMAT_GEN_FUNC(g)
	_BOOST_CMAT_GEN_FUNC(b)
	_BOOST_CMAT_GEN_FUNC(a)
};

#undef _BOOST_CMAT_GEN_FUNC_1
#undef _BOOST_CMAT_GEN_FUNC

//--------------------------------------------------------------------------------------------------------
// Specialisation
//--------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------
// --- General matrix

template<class T,int R,int C,class F=SDenseStorage<T,R,C>>
class CMat : public CMatBase<R,C,F,CMat<T,R,C,F>>
{
private:
	typedef CMatBase<R,C,F,CMat<T,R,C,F>> Base;

public:
	CMat(const F &storage,MatrixInit init=MatrixInit::Null) : Base(storage,init) {}
	CMat(MatrixInit init=MatrixInit::Null) : Base(init) {}
	template<typename... A> explicit CMat(const F &storage,T val,A... arg) : Base(storage,val,arg...) {}
	template<typename... A> explicit CMat(T val,A... arg) : Base(val,arg...) {}
	CMat(const CMat &obj) : Base(obj) {}
	template<class F2> CMat(const CMat<T,R,C,F2> &obj) : Base(obj) {}
	CMat& operator=(const CMat &obj) { Base::operator=(obj);return(*this); }
};

template<class T,int R,int C,class F,class D>
CMat<T,R,C,F> make_matrix(D &o)
{
	return(CMat<T,R,C,F>(o));
}

template<class T,int R,int C>
CMat<T,R,C,SProxyArrayStorage<T,R,C>> make_matrix(T(&tab)[R][C])
{
	return(CMat<T,R,C,SProxyArrayStorage<T,R,C>>(tab,MatrixInit::No));
}

template<class T,int R>
CMat<T,R,1,SProxyColumnStorage<T,R,1>> make_matrix(T(&tab)[R])
{
	return(CMat<T,R,1,SProxyColumnStorage<T,R,1>>(tab,MatrixInit::No));
}

template<int R,int C,class T,int N>
CMat<T,R,C,SProxyColumnStorage<T,R,C>> make_matrix(T(&tab)[N])
{
	static_assert(R*C==N,"Table size (N) must be egal to R*C (template parameters)");
	return(CMat<T,R,C,SProxyColumnStorage<T,R,C>>(tab,MatrixInit::No));
}

template<int R,class T>
CMat<T,R,1,SProxyColumnStorage<T,R,1>> make_matrix(T *tab)
{
	return(CMat<T,R,1,SProxyColumnStorage<T,R,1>>(tab,MatrixInit::No));
}

template<int R,int C,class T>
typename std::enable_if<is_carray<T>::value && !is_carray<T>::staticvalue,CMat<typename is_carray<T>::valuetype,R,C,SProxyColumnStorage<typename is_carray<T>::valuetype,R,C>>>::type make_matrix(T tab)
{
	return(CMat<typename is_carray<T>::valuetype,R,C,SProxyColumnStorage<typename is_carray<T>::valuetype,R,C>>(tab,MatrixInit::No));
}

//--------------------------------------------------------------------------------------------------------
// --- Vectors

template<class T,int R,class F>
class CMat<T,R,1,F> : public CMatBase<R,1,F,CMat<T,R,1,F>>,public CVecBase<T,R,CMat<T,R,1,F>>
{
	template<class T1,int R1,typename Derived1>
	friend class CVecBase;

private:
	typedef CMatBase<R,1,F,CMat<T,R,1,F>> Base;

public:
	CMat(const F &storage,MatrixInit init=MatrixInit::Null) : Base(storage,init) {}
	CMat(MatrixInit init=MatrixInit::Null) : Base(init) {}
	template<typename... A> explicit CMat(const F &storage,T val,A... arg) : Base(storage,val,arg...) {}
	template<typename... A> explicit CMat(T val,A... arg) : Base(val,arg...) {}
	CMat(const CMat &obj) : Base(obj) {}
	template<class F2> CMat(const CMat<T,R,1,F2> &obj) : Base(obj) {}
	CMat& operator=(const CMat &obj) { Base::operator=(obj);return(*this); }
};

template<class T,int R,class F>
class CMat<T,1,R,F> : public CMatBase<1,R,F,CMat<T,1,R,F>>,public CVecBase<T,R,CMat<T,1,R,F>>
{
	template<class T1,int R1,typename Derived1>
	friend class CVecBase;

private:
	typedef CMatBase<1,R,F,CMat<T,1,R,F>> Base;

public:
	CMat(const F &storage,MatrixInit init=MatrixInit::Null) : Base(storage,init) {}
	CMat(MatrixInit init=MatrixInit::Null) : Base(init) {}
	template<typename... A> explicit CMat(const F &storage,T val,A... arg) : Base(storage,val,arg...) {}
	template<typename... A> explicit CMat(T val,A... arg) : Base(val,arg...) {}
	CMat(const CMat &obj) : Base(obj) {}
	template<class F2> CMat(const CMat<T,1,R,F2> &obj) : Base(obj) {}
	CMat& operator=(const CMat &obj) { Base::operator=(obj);return(*this); }
};

template<class T,int R,class F=SDenseStorage<T,R,1>>
using CVec=CMat<T,R,1,F>;

template<class T,int R,class F=SDenseStorage<T,1,R>>
using CVecT=CMat<T,1,R,F>;

//--------------------------------------------------------------------------------------------------------
// --- Square matrix

template<class T,int N,class F>
class CMat<T,N,N,F> : public CMatSquare<N,F,CMat<T,N,N,F>>
{
private:
	typedef CMatSquare<N,F,CMat<T,N,N,F>> Base;

public:
	CMat(const F &storage,MatrixInit init=MatrixInit::Null) : Base(storage,init) {}
	CMat(MatrixInit init=MatrixInit::Null) : Base(init) {}
	CMat(const F &storage,MatrixSquareInit init) : Base(storage,init) {}
	CMat(MatrixSquareInit init) : Base(init) {}
	template<typename... A> explicit CMat(const F &storage,T val,A... arg) : Base(storage,val,arg...) {}
	template<typename... A> explicit CMat(T val,A... arg) : Base(val,arg...) {}
	CMat(const CMat &obj) : Base(obj) {}
	template<class F2> CMat(const CMat<T,N,N,F2> &obj) : Base(obj) {}
	CMat& operator=(const CMat &obj) { Base::operator=(obj);return(*this); }
};

//--------------------------------------------------------------------------------------------------------
// --- Degenerated matrix

template<class T,class F>
class CMat<T,1,1,F> : public CMatSquare<1,F,CMat<T,1,1,F>>,public CVecBase<T,1,CMat<T,1,1,F>>
{
	template<class T1,int R1,typename Derived1>
	friend class CVecBase;

private:
	typedef CMatSquare<1,F,CMat<T,1,1,F>> Base;

public:
	CMat(const F &storage,MatrixInit init=MatrixInit::Null) : Base(storage,init) {}
	CMat(MatrixInit init=MatrixInit::Null) : Base(init) {}
	CMat(const F &storage,MatrixSquareInit init) : Base(storage,init) {}
	CMat(MatrixSquareInit init) : Base(init) {}
	template<typename... A> explicit CMat(const F &storage,T val,A... arg) : Base(storage,val,arg...) {}
	template<typename... A> explicit CMat(T val,A... arg) : Base(val,arg...) {}
	CMat(const CMat &obj) : Base(obj) {}
	template<class F2> CMat(const CMat<T,1,1,F2> &obj) : Base(obj) {}
	CMat& operator=(const CMat &obj) { Base::operator=(obj);return(*this); }
};

//--------------------------------------------------------------------------------------------------------
// --- Views

struct MView
{
	// Swizzle views

	template<class T> struct SwizzleGenerator { typedef SVectorDim<T> type; };
	template<typename... A> using GenSwizzleTypeList=GenTypeListGenerator<SwizzleGenerator,A...>;

	template<int I> struct SwizzleGeneratorI { typedef SVectorDimI<I> type; };
	template<int... I> using GenSwizzleIntegralList=GenIntegralListGenerator<SwizzleGeneratorI,I...>;

	template<class List,class T,int R,class F>
	inline static CMat<T,ListCount<List>::value,1,SSwizzleStorage<T,ListCount<List>::value,R,F,List>> SwizzleL(CMat<T,R,1,F> &mat)
	{
		return(CMat<T,ListCount<List>::value,1,SSwizzleStorage<T,ListCount<List>::value,R,F,List>>(
			SSwizzleStorage<T,ListCount<List>::value,R,F,List>(mat.Storage),MatrixInit::No)
		);
	}

	template<typename S1,typename S2,class T,int R,class F>
	inline static auto Swizzle(CMat<T,R,1,F> &mat) -> decltype(SwizzleL<GenSwizzleTypeList<S1,S2>>(mat)) { return(SwizzleL<GenSwizzleTypeList<S1,S2>>(mat)); }
	template<typename S1,typename S2,typename S3,class T,int R,class F>
	inline static auto Swizzle(CMat<T,R,1,F> &mat) -> decltype(SwizzleL<GenSwizzleTypeList<S1,S2,S3>>(mat)) { return(SwizzleL<GenSwizzleTypeList<S1,S2,S3>>(mat)); }
	template<typename S1,typename S2,typename S3,typename S4,class T,int R,class F>
	inline static auto Swizzle(CMat<T,R,1,F> &mat) -> decltype(SwizzleL<GenSwizzleTypeList<S1,S2,S3,S4>>(mat)) { return(SwizzleL<GenSwizzleTypeList<S1,S2,S3,S4>>(mat)); }

	template<int S1,int S2,class T,int R,class F>
	inline static auto Swizzle(CMat<T,R,1,F> &mat) -> decltype(SwizzleL<GenSwizzleIntegralList<S1,S2>>(mat)) { return(SwizzleL<GenSwizzleIntegralList<S1,S2>>(mat)); }
	template<int S1,int S2,int S3,class T,int R,class F>
	inline static auto Swizzle(CMat<T,R,1,F> &mat) -> decltype(SwizzleL<GenSwizzleIntegralList<S1,S2,S3>>(mat)) { return(SwizzleL<GenSwizzleIntegralList<S1,S2,S3>>(mat)); }
	template<int S1,int S2,int S3,int S4,class T,int R,class F>
	inline static auto Swizzle(CMat<T,R,1,F> &mat) -> decltype(SwizzleL<GenSwizzleIntegralList<S1,S2,S3,S4>>(mat)) { return(SwizzleL<GenSwizzleIntegralList<S1,S2,S3,S4>>(mat)); }

	// Math views

	template<class T,int R,int C,class F>
	inline static CMat<T,C,R,STransposeStorage<T,C,R,F>> Transpose(CMat<T,R,C,F> &mat)
	{
		return(CMat<T,C,R,STransposeStorage<T,C,R,F>>(STransposeStorage<T,C,R,F>(mat.Storage),MatrixInit::No));
	}

	// Submatrix views

	template<int R,int C,int H,int W,class T,int R1,int C1,class F>
	inline static CMat<T,H,W,SViewMatrixStorage<T,R,C,H,W,R1,C1,F>> Matrix(CMat<T,R1,C1,F> &mat)
	{
		static_assert(R>=0,"First template parameter (start row) must be greater or egal to 0.");
		static_assert(C>=0,"Second template parameter (start column) must be greater or egal to 0.");
		static_assert(R+H-1<R1,"End row of the submatrix (R+H-1) must be less than the row dimension of the initial matrix.");
		static_assert(C+W-1<C1,"End column of the submatrix (C+W-1) must be less than the column dimension of the initial matrix.");
		static_assert(H>0,"Row dimension of the submatrix must be greater than 0.");
		static_assert(W>0,"Column dimension of the submatrix must be greater than 0.");
		static_assert(H<=R1,"Row dimension of the submatrix must be less or egal to the row dimension of the initial matrix.");
		static_assert(W<=C1,"Column dimension of the submatrix must be less or egal to the column dimension of the initial matrix.");
		return(CMat<T,H,W,SViewMatrixStorage<T,R,C,H,W,R1,C1,F>>(SViewMatrixStorage<T,R,C,H,W,R1,C1,F>(mat.Storage),MatrixInit::No));
	}

	template<int C,class T,int R1,int C1,class F>
	inline static CMat<T,R1,1,SViewMatrixStorage<T,0,C,R1,1,R1,C1,F>> Col(CMat<T,R1,C1,F> &mat)
	{
		static_assert(C>=0,"First template parameter (start column) must be greater or egal to 0.");
		static_assert(C<C1,"First template parameter (start column) must be less than the column dimension of the initial matrix.");
		return(CMat<T,R1,1,SViewMatrixStorage<T,0,C,R1,1,R1,C1,F>>(SViewMatrixStorage<T,0,C,R1,1,R1,C1,F>(mat.Storage),MatrixInit::No));
	}

	template<int C,int RBEG,int REND,class T,int R1,int C1,class F>
	inline static CMat<T,REND-RBEG+1,1,SViewMatrixStorage<T,RBEG,C,REND-RBEG+1,1,R1,C1,F>> Col(CMat<T,R1,C1,F> &mat)
	{
		static_assert(C>=0,"First template parameter (start column) must be greater or egal to 0.");
		static_assert(C<C1,"First template parameter (start column) must be less than the column dimension of the initial matrix.");
		static_assert(RBEG>=0,"Second template parameter must be greater or egal to 0 (RBEG>=0).");
		static_assert(REND>=RBEG,"Third template parameter must be greater or egal to the second template parameter (REND>=RBEG).");
		static_assert(REND<R1,"Third template parameter must be less than the row dimension of the matrix.");
		return(CMat<T,REND-RBEG+1,1,SViewMatrixStorage<T,RBEG,C,REND-RBEG+1,1,R1,C1,F>>(SViewMatrixStorage<T,RBEG,C,REND-RBEG+1,1,R1,C1,F>(mat.Storage),MatrixInit::No));
	}

	template<int R,class T,int R1,int C1,class F>
	inline static CMat<T,1,C1,SViewMatrixStorage<T,R,0,1,C1,R1,C1,F>> Row(CMat<T,R1,C1,F> &mat)
	{
		static_assert(R>=0,"First template parameter (start row) must be greater or egal to 0.");
		static_assert(R<R1,"First template parameter (start row) must be less than the row dimension of the initial matrix.");
		return(CMat<T,1,C1,SViewMatrixStorage<T,R,0,1,C1,R1,C1,F>>(SViewMatrixStorage<T,R,0,1,C1,R1,C1,F>(mat.Storage),MatrixInit::No));
	}

	template<int R,int CBEG,int CEND,class T,int R1,int C1,class F>
	inline static CMat<T,1,CEND-CBEG+1,SViewMatrixStorage<T,R,CBEG,1,CEND-CBEG+1,R1,C1,F>> Row(CMat<T,R1,C1,F> &mat)
	{
		static_assert(R>=0,"First template parameter (start row) must be greater or egal to 0.");
		static_assert(R<R1,"First template parameter (start row) must be less than the row dimension of the initial matrix.");
		static_assert(CBEG>=0,"Second template parameter must be greater or egal to 0 (CBEG>=0).");
		static_assert(CEND>=CBEG,"Third template parameter must be greater or egal to the second template parameter (CEND>=CBEG).");
		static_assert(CEND<C1,"Third template parameter must be less than the column dimension of the matrix.");
		return(CMat<T,1,CEND-CBEG+1,SViewMatrixStorage<T,R,CBEG,1,CEND-CBEG+1,R1,C1,F>>(SViewMatrixStorage<T,R,CBEG,1,CEND-CBEG+1,R1,C1,F>(mat.Storage),MatrixInit::No));
	}

	template<class T,int N,class F>
	inline static CMat<T,N,1,SViewDiagonalStorage<T,N,F>> Diag(CMat<T,N,N,F> &mat)
	{
		return(CMat<T,N,1,SViewDiagonalStorage<T,N,F>>(SViewDiagonalStorage<T,N,F>(mat.Storage),MatrixInit::No));
	}
};

// Swizzle operator

template<typename... A> struct SWT {};
template<int... I> struct SWI {};

template<class T,int R,class F,typename... A>
inline auto operator%(CMat<T,R,1,F> &mat,SWT<A...>) -> decltype(MView::SwizzleL<MView::GenSwizzleTypeList<A...>>(mat)) { return(MView::SwizzleL<MView::GenSwizzleTypeList<A...>>(mat)); }

template<class T,int R,class F,int... I>
inline auto operator%(CMat<T,R,1,F> &mat,SWI<I...>) -> decltype(MView::SwizzleL<MView::GenSwizzleIntegralList<I...>>(mat)) { return(MView::SwizzleL<MView::GenSwizzleIntegralList<I...>>(mat)); }

template<typename... A> SWT<A...> SW() { return(SWT<A...>()); }
template<int... I> SWI<I...> SW() { return(SWI<I...>()); }

template<class T,int R,class F,typename... A>
inline auto operator%(CMat<T,R,1,F> &mat,SWT<A...> (&)()) -> decltype(MView::SwizzleL<MView::GenSwizzleTypeList<A...>>(mat)) { return(MView::SwizzleL<MView::GenSwizzleTypeList<A...>>(mat)); }

template<class T,int R,class F,int... I>
inline auto operator%(CMat<T,R,1,F> &mat,SWI<I...> (&)()) -> decltype(MView::SwizzleL<MView::GenSwizzleIntegralList<I...>>(mat)) { return(MView::SwizzleL<MView::GenSwizzleIntegralList<I...>>(mat)); }

// Generation of XX, XY, X1 ... functions for the swizzle operator
#ifdef GEN_SWIZZLE_FUNCTION

#define GEN_SWIZZLE_FUNCTION_3(r, data, i, elem) BOOST_PP_COMMA_IF(i) BOOST_PP_CAT(data ,BOOST_PP_CAT(elem,data))
#define GEN_SWIZZLE_FUNCTION_2(func,param) SWT<param> (&func)()=SW<param>;
#define GEN_SWIZZLE_FUNCTION_1(r, product) GEN_SWIZZLE_FUNCTION_2(BOOST_PP_SEQ_CAT(product),BOOST_PP_SEQ_FOR_EACH_I(GEN_SWIZZLE_FUNCTION_3, _, product))

#define GEN_SWIZZLE_FUNCTION_S1 (R)(G)(B)(A)
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1))
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1))
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1))
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1))
#undef GEN_SWIZZLE_FUNCTION_S1

#define GEN_SWIZZLE_FUNCTION_S1 (X)(Y)(Z)(W)
#define GEN_SWIZZLE_FUNCTION_S2 GEN_SWIZZLE_FUNCTION_S1(0)(1)
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1))
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S2))
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S2))
BOOST_PP_SEQ_FOR_EACH_PRODUCT(GEN_SWIZZLE_FUNCTION_1,(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S1)(GEN_SWIZZLE_FUNCTION_S2))
#undef GEN_SWIZZLE_FUNCTION_S2
#undef GEN_SWIZZLE_FUNCTION_S1

#undef GEN_SWIZZLE_FUNCTION_3
#undef GEN_SWIZZLE_FUNCTION_2
#undef GEN_SWIZZLE_FUNCTION_1

#endif

//--------------------------------------------------------------------------------------------------------
// General operations
//--------------------------------------------------------------------------------------------------------

template<int R,int C,class F,typename Derived>
CMat<typename F::value_type,C,R> Transpose(const CMatBase<R,C,F,Derived> &mat);

template<int R,int C,class F1,typename Derived1,class F2,typename Derived2,class F>
auto Operator(const CMatBase<R,C,F1,Derived1> &mat1,const CMatBase<R,C,F2,Derived2> &mat2,F func) -> CMat<decltype(func(mat1.GetVal(0),mat2.GetVal(0))),R,C>;

template<int R,int C,class F1,typename D1,class F2,typename D2>
inline auto operator+(const CMatBase<R,C,F1,D1> &mat1,const CMatBase<R,C,F2,D2> &mat2) -> CMat<decltype(mat1.GetVal(0)+mat2.GetVal(0)),R,C> { return(Operator(mat1,mat2,[](const typename F1::value_type &v1,const typename F2::value_type &v2) { return(v1+v2); })); }

template<int R,int C,class F1,typename D1,class F2,typename D2>
inline auto operator-(const CMatBase<R,C,F1,D1> &mat1,const CMatBase<R,C,F2,D2> &mat2) -> CMat<decltype(mat1.GetVal(0)-mat2.GetVal(0)),R,C> { return(Operator(mat1,mat2,[](const typename F1::value_type &v1,const typename F2::value_type &v2) { return(v1-v2); })); }

template<int R,int C,class F,typename Derived>
inline CMat<typename F::value_type,R,C> operator-(const CMatBase<R,C,F,Derived> &mat) { CMat<typename F::value_type,R,C> res(MatrixInit::No);Loop<R*C>::Do([&](unsigned int i){ res(i)=-mat(i); });return(res); }

template<int R1,int N,class F1,typename Derived1,int C2,class F2,typename Derived2>
auto operator*(const CMatBase<R1,N,F1,Derived1> &mat1,const CMatBase<N,C2,F2,Derived2> &mat2) -> CMat<decltype(mat1(0,0)*mat2(0,0)),R1,C2>;

template<int N,class F1,typename Derived1,class F2,typename Derived2>
auto operator*(const CMatBase<1,N,F1,Derived1> &mat1,const CMatBase<N,1,F2,Derived2> &mat2) -> decltype(mat1.GetVal(0)*mat2.GetVal(0));

template<int R,class F1,typename Derived1,class F2,typename Derived2>
inline auto Dot(const CMatBase<R,1,F1,Derived1> &v1,const CMatBase<R,1,F2,Derived2> &v2) -> decltype(v1.GetVal(0)*v2.GetVal(0)) { decltype(v1.GetVal(0)*v2.GetVal(0)) val=0;Loop<R>::Do([&](unsigned int i){ val+=v1.GetVal(i)*v2.GetVal(i); });return(val); }

//--------------------------------------------------------------------------------------------------------
// Comparison operators

template<int R1,int C1,class F1,typename Derived1,int R2,int C2,class F2,typename Derived2>
inline bool operator==(const CMatBase<R1,C1,F1,Derived1>&,const CMatBase<R2,C2,F2,Derived2>&) { return(false); }

template<int R,int C,class F1,typename Derived1,class F2,typename Derived2>
bool operator==(const CMatBase<R,C,F1,Derived1> &obj1,const CMatBase<R,C,F2,Derived2> &obj2) { return(obj1.Storage==obj2.Storage); }

template<int R1,int C1,class F1,typename Derived1,int R2,int C2,class F2,typename Derived2>
inline bool operator!=(const CMatBase<R1,C1,F1,Derived1> &obj1,const CMatBase<R2,C2,F2,Derived2> &obj2) { return(!(obj1==obj2)); }

}

#include "Impl/CMat_Impl.hpp"

#endif
